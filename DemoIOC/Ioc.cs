﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DemoIOC
{
    internal class Ioc
    {
        private Dictionary<Type, object> services = new Dictionary<Type, object>();

        public T Get<T>()
            where T : class
        {
            object instance = services[typeof(T)];
            if(instance == null)
            {
                // creér l'instance
                ConstructorInfo ctor = typeof(T).GetConstructors().First();

                object[] paramters = ctor.GetParameters().Select(p =>
                {
                    MethodInfo method = typeof(Ioc).GetMethod("Get");
                    method = method.MakeGenericMethod(p.ParameterType);
                    return method.Invoke(this, new object[0]);
                }).ToArray();

                instance = ctor.Invoke(paramters);
                services[typeof(T)] = instance;
            }
            return (T)instance;

        }

        public void RegisterSingleton<T>()
            where T : class
        {
            services.Add(typeof(T), null);
        }
    }
}
