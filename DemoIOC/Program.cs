﻿using DemoIOC.Controllers;
using DemoIOC.Services;
using System;

namespace DemoIOC
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Ioc services = new Ioc();

            services.AddControllers();
            services.AddServices();

            //services.RegisterSingleton<ServiceA>();
            //services.RegisterSingleton<ServiceB>();
            //services.RegisterSingleton<ServiceC>();

            ServiceA sa = services.Get<ServiceA>();
            ControllerA ca = services.Get<ControllerA>();
            ControllerB cb = services.Get<ControllerB>();
        }
    }
}
