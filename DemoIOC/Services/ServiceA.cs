﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoIOC.Services
{
    class ServiceA
    {
        private readonly ServiceB _serviceB;
        public ServiceA(ServiceB serviceB)
        {
            _serviceB = serviceB;
        }
    }
}
