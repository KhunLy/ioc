﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoIOC.Services
{
    class ServiceB
    {
        private readonly ServiceC _serviceC;
        public ServiceB(ServiceC serviceC)
        {
            _serviceC = serviceC;
        }
    }
}
