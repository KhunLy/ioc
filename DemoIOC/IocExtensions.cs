﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DemoIOC
{
    internal static class IocExtensions
    {
        public static void AddControllers(this Ioc ioc)
        {
            Assembly.GetCallingAssembly()
                //.GetTypes().Where(t => t.IsAssignableTo(typeof(ControllerBase))).ToList().ForEach(t =>
                .GetTypes().Where(t => t.Namespace.EndsWith("Controllers")).ToList().ForEach(t =>
                {
                    typeof(Ioc).GetMethod("RegisterSingleton").MakeGenericMethod(t).Invoke(ioc, new object[0]);
                });
        }

        public static void AddServices(this Ioc ioc)
        {
            Assembly.GetCallingAssembly()
                .GetTypes().Where(t => t.Namespace.EndsWith("Services")).ToList().ForEach(t =>
                {
                    typeof(Ioc).GetMethod("RegisterSingleton").MakeGenericMethod(t).Invoke(ioc, new object[0]);
                });
        }
    }
}
